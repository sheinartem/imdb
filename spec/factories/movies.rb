FactoryBot.define do
  factory :movie do
    category
    title { 'the Avengers' }
    text { 'very good movie' }
  end
end