require 'rails_helper'

RSpec.describe CategoriesController, type: :controller do
  describe "GET index" do
    let(:category1) { create(:category, name: 'Horror') }
    let(:category2) { create(:category, name: 'Action') } 

    it 'assigns @categories' do
      get :index
      expect(assigns(:categories)).to eq([category1, category2])
    end
    
    it "has a 200 status code" do
      get :index
      expect(response.status).to eq(200)
    end
  end

  describe 'GET show' do
    let(:category) { create(:category, name: 'Action') }
    let(:user) { create(:user, email: 'test1@gmail.com') }

    it 'assigns variables' do
      allow(controller).to receive(:current_user).and_return(user)

      get :show, params: {id: category.id}
      expect(assigns(:category)).to eq(category)
    end

    it "has a 200 status code" do
      allow(controller).to receive(:current_user).and_return(user)

      get :show, params: {id: category.id}
      expect(response.status).to eq(200)
    end
  end

  describe 'GET new' do
    let!(:category) { create(:category, name: 'Horror') }

    it 'assigns @categorie' do
      get :new
      expect(assigns(:category)).to be_a(Category)
    end
    
    it "has a 200 status code" do
      get :new
      expect(response.status).to eq(200)
    end
  end

  describe 'POST create' do

    it 'creates movie' do
      expect(Category.first).to be_nil
      post :create, params: {category: {name: 'Action'} } 
      expect(Category.where(name: 'Action').first).not_to be_nil
    end

    it "has a 302 status code" do
      post :create, params: {category: {name: 'Action'} } 
      expect(response.status).to eq(302)
    end
  end

  describe 'PUT update' do  
    let!(:category) { create(:category, name: 'Horror') }

    it 'updates movie' do
      patch :update, params: {id: category.id, category: {name: 'Action'}} 
      expect(Category.first.name).not_to eq(category.name)
    end
  end

  describe 'DELETE destroy' do
    let!(:category) { create(:category, name: 'Horror') }

    it 'deletes movie' do
      delete :destroy, params: {id: category.id} 
      expect(Category.first).to be_nil
    end
  end
end
