require 'rails_helper'

RSpec.describe MoviesController, type: :controller do
  describe "GET index" do
    let(:category1) { create(:category, name: 'Horror') }
    let(:category2) { create(:category, name: 'Action') }
    let!(:movie1) { create(:movie, category: category1, title: 'Harry Potter1') }
    let!(:movie2) { create(:movie, category: category2, title: 'Harry Potter2') }
    let!(:movie3) { create(:movie, category: category2, title: 'Harry Potter3') }

    it 'assigns @movies' do
      get :index
      expect(assigns(:movies)).to eq([movie1, movie2, movie3])
    end

    it 'renders the index template' do
      get :index
      expect(response).to render_template('index')
    end

    it 'assigns @movies by category' do
      get :index, params: {category_id: category1.id}
      expect(assigns(:movies)).to eq([movie1])
    end

    it 'fills dictionaries' do 
      get :index
      expect(assigns(:categories)).to match_array([category2, category1])
    end
    
    it "has a 200 status code" do
      get :index
      expect(response.status).to eq(200)
    end
  end

  describe 'GET show' do
    let(:movie) { create(:movie, title: 'Harry Potter') }
    let(:user) { create(:user, email: 'test1@gmail.com') }
    let!(:rating) { create(:rating, movie: movie, user: user, rating: 1) }

    it 'assigns variables' do
      allow(controller).to receive(:current_user).and_return(user)

      get :show, params: {id: movie.id}
      expect(assigns(:movie)).to eq(movie)
      expect(assigns(:rating)).to eq(rating)
    end

    it "has a 200 status code" do
      allow(controller).to receive(:current_user).and_return(user)

      get :show, params: {id: movie.id}
      expect(response.status).to eq(200)
    end
  end

  describe 'GET new' do
    let!(:category) { create(:category, name: 'Horror') }

    it 'assigns @movie' do
      get :new
      expect(assigns(:movie)).to be_a(Movie)
    end

    it 'fills dictionaries' do 
      get :new
      expect(assigns(:categories)).to eq([category])
    end
    
    it "has a 200 status code" do
      get :new
      expect(response.status).to eq(200)
    end
  end

  describe 'POST create' do
    let!(:category) { create(:category, name: 'Horror') }

    it 'creates movie' do
      expect(Movie.first).to be_nil
      post :create, params: {movie: {title: 'Harry Potter', category_id: category.id, text: 'Hello'} } 
      expect(Movie.where(title: 'Harry Potter').first).not_to be_nil
    end

    it "has a 302 status code" do
      post :create, params: {movie: {title: 'Harry Potter', category_id: category.id, text: 'Hello'} } 
      expect(response.status).to eq(302)
    end
  end

  describe 'PUT update' do  
    let!(:movie) { create(:movie, category: category, title: 'Harry Potter', text: 'Hello') }
    let!(:category) { create(:category, name: 'Horror') }

    it 'updates movie' do
      patch :update, params: {id: movie.id, movie: {title: 'Harry', category_id: category.id, text: 'Hello'}} 
      expect(Movie.first.title).not_to eq(movie.title)
    end
  end

  describe 'DELETE destroy' do
    let!(:movie) { create(:movie, category: category, title: 'Harry Potter', text: 'Hello') }
    let!(:category) { create(:category, name: 'Horror') }

    it 'deletes movie' do
      delete :destroy, params: {id: movie.id} 
      expect(Movie.first).to be_nil
    end
  end
end