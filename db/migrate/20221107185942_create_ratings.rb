class CreateRatings < ActiveRecord::Migration[6.1]
  def change
    create_table :ratings do |t|
      t.references :user, foreign_key: true
      t.references :movie, foreign_key: true
      t.integer :rating 

      t.timestamps
    end
    add_index :ratings, [:user_id, :movie_id], unique: true
  end
end
