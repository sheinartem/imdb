class MoviesController < ApplicationController
    def index
        fill_dictionaries
        if params[:category_id]
            @movies = Movie.where(category_id: params[:category_id]).page(params[:page])
        else
            @movies = Movie.all.page(params[:page])
        end
    end

    def show
        @movie = Movie.find(params[:id])
        @rating = Rating.where(user: current_user, movie: @movie).first
    end

    def new
        @movie = Movie.new
        fill_dictionaries
    end

    def create
        @movie = Movie.new(movie_params)
    
        if @movie.save
          redirect_to @movie
        else
          render :new
        end
    end

    def edit
        @movie = Movie.find(params[:id])
        fill_dictionaries
    end
    
    def update
        @movie = Movie.find(params[:id])
        
        if @movie.update(movie_params)
            redirect_to @movie
        else
            render :edit
        end
    end 

    def destroy
        @movie = Movie.find(params[:id])
        @movie.destroy
    
        redirect_to movies_path
    end

    def set_rating
       Rating.create(user_id: current_user.id, movie_id: params[:id], rating: movie_params[:rating])
       redirect_to movie_path
    end

    def filter
        if params[:category].empty?
            redirect_to movies_path
        else
            redirect_to movies_path(category_id: params[:category])
        end
    end

    private
    
    def movie_params
      params.require(:movie).permit(:category_id, :title, :rating, :text)
    end

    def fill_dictionaries
        @categories = Category.all
    end
end
