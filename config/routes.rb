Rails.application.routes.draw do
  root to: 'movies#index'

  resources :movies do
    post 'set_rating', on: :member
    post 'filter', on: :collection
  end

  resources :categories
  resources :ratings
  devise_for :users


  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
