# == Schema Information
#
# Table name: movies
#
#  id          :bigint           not null, primary key
#  text        :string
#  title       :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  category_id :bigint
#
# Indexes
#
#  index_movies_on_category_id  (category_id)
#
# Foreign Keys
#
#  fk_rails_...  (category_id => categories.id)
#
class Movie < ActiveRecord::Base
    belongs_to :category
    has_many :ratings, dependent: :destroy

    def average_rating
        ratings.average(:rating)
    end

end
