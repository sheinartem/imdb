require 'rails_helper'

RSpec.describe HomePageController, type: :controller do
  describe "GET index" do
    let(:category1) { create(:category, name: 'Horror') }
    let(:category2) { create(:category, name: 'Action') }
    let!(:movie1) { create(:movie, category: category1, title: 'Harry Potter1') }
    let!(:movie2) { create(:movie, category: category2, title: 'Harry Potter2') }
    let!(:movie3) { create(:movie, category: category2, title: 'Harry Potter3') }

    it 'assigns @movies' do
      get :index
      expect(assigns(:movies)).to eq([movie1, movie2, movie3])
    end
    
    it "has a 200 status code" do
      get :index
      expect(response.status).to eq(200)
    end
  end
end
