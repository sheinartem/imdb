FactoryBot.define do
  factory :rating do
    movie
    user
    rating { 1 }
  end       
end    