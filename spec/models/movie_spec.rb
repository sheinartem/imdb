# == Schema Information
#
# Table name: movies
#
#  id          :bigint           not null, primary key
#  text        :string
#  title       :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  category_id :bigint
#
# Indexes
#
#  index_movies_on_category_id  (category_id)
#
# Foreign Keys
#
#  fk_rails_...  (category_id => categories.id)
#
require 'rails_helper'

RSpec.describe Movie, type: :model do
  describe 'associations' do
    it { is_expected.to belong_to(:category) }
  end

  describe 'average_rating' do
    let(:category) { create(:category, name: 'Horror') }
    let(:movie) { create(:movie, category: category, title: 'Harry Potter') }
    let(:user1) { create(:user, email: 'test1@gmail.com') }
    let(:user2) { create(:user, email: 'test2@gmail.com') }
    let(:user3) { create(:user, email: 'test3@gmail.com') }
    let!(:rating1) { create(:rating, movie: movie, user: user1, rating: 1) }
    let!(:rating2) { create(:rating, movie: movie, user: user2, rating: 4) }
    let!(:rating3) { create(:rating, movie: movie, user: user3, rating: 7) }

    it 'calculate rating' do
      expect(movie.average_rating).to eq(4)
    end

    describe 'more test' do
      let!(:rating3) { create(:rating, movie: movie, user: user3, rating: 10) }

      it 'calculate rating2' do
        expect(movie.average_rating).to eq(5)
      end  
    end
  end
end
